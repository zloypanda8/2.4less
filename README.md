Некоторые нюансы решения:
1. В отдельном скрипте get_artifacts.sh нахожу джобу со сборкой приложения и стягиваю оттуда артифакт.
2. В gitlab-ci указал, куда запихнуть артифакт. Теперь его можно скачать из браузера. Или как угодно


**Комментарий по less2.7**
0. Делал всё по доке
1. Создал ключи из под gitlab-runner
2. Запихнул приватный ключ в переменную
3. Добавил переменную в gitlab-ci
```
  - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
```

4. После запуска джобы нашёл склонированную репу с ридми приватного проекта:
```
[gitlab-runner@centos ~]$ cat builds/XXXXXX/0/zloypanda8/2.4less/less2.7/README.md
# less2.7
Mr. Tusk...why do you blubber so
```

